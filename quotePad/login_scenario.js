/**
* Login and set cookies
*/
import { check, sleep } from "k6";
import { loginToApp } from "./scripts/api/login.js";
import { baseParams } from "./scripts/api/param/param.js";

/**
* Login and check the correct status
*/
export function login_scenario(environment, branch, user) {
  let login_response = loginToApp(environment, branch, user);

  const token = login_response.cookies["auth-token"][0].value;
  const arrow = login_response.cookies["Arrow"][0].value;

  var PARAM =  baseParams(token, arrow);
  check(login_response, {
    "status is 204": (r) => r.status === 204,
  });
}
