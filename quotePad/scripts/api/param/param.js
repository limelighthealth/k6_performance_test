/**
* Performs the POST for logging in only. Does not set cookies in TestSession.
* @param token token
* @param arrow arrow
* @returns json format to set params
*/
export function baseParams(token, arrow) {
  const params = {
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return params;
}
