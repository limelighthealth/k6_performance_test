/**
* Login API for authentication
*/
import http from "k6/http";
import { baseParams } from "./param/param.js";

const urlLogin = "/api/user/login";
const users = JSON.parse(open("../../testData/users.json"));
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Performs the POST for logging in only. Does not set cookies in TestSession.
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param user user role to get user and password information 
* @returns json file with the auth-token and Arrow information
*/
export function loginToApp(enviroment, branch, user){
  const userAndPassFile =users[enviroment][user];
  const userAndPass = {
    email: userAndPassFile.username ,
    password: userAndPassFile.password
  };
  const urlData = enviromentURL[enviroment][branch] + urlLogin;
  const login_response = http.post(urlData, userAndPass);
  return login_response;
}

/**
* Performs the POST for logging and set cookies
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param user user role to get user and password information 
* @returns json file with the auth-token and Arrow information
*/
export function loginWithCookies(enviroment, branch, user){
  const userAndPassFile =users[enviroment][user];
  const userAndPass = {
    email: userAndPassFile.username ,
    password: userAndPassFile.password
  };
  const urlData = enviromentURL[enviroment][branch] + urlLogin;
  const login_response = http.post(urlData, userAndPass);
  const token = login_response.cookies["auth-token"][0].value;
  const arrow = login_response.cookies["Arrow"][0].value;

  const params = baseParams(token, arrow);
  return params;
}
