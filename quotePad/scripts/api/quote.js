/**
* A set of Quote APIs.
*/
import http from "k6/http";

const urlGroup = "/v1/group/";
const urlQuote = "/v1/quote/";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Gets all quote objects for the specified group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getQuoteByGroupId(enviroment, branch, params, groupId){
    const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/quotes";
    return http.get(urlData, params);
}

/**
* Gets a quote object for the specified id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param quoteId quote id
* @return http result information
*/
export function getQuote(enviroment, branch, params, quoteId){
  const urlData = enviromentURL[enviroment][branch] + urlQuote + quoteId;
  return http.get(urlData, params);
}

/**
* Create a quote with the specified parameters
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param quoteData quote data information
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function createQuote(enviroment, branch, quoteData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/quote";
  return http.post(urlData, quoteData, params);
}

/**
* Decline to Quote a group or subset of product lines
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param quoteData quote data information
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function postDeclineQuote(enviroment, branch, quoteData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/decline-to-quote";
  return http.post(urlData, quoteData, params);
}

/**
* Update select data on an existing quote
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param quoteData quote data information
* @param params information for authentication
* @param quoteId quote id
* @return http result information
*/
export function putQuote(enviroment, branch, quoteData, params, quoteId){
  const urlData = enviromentURL[enviroment][branch] + urlQuote + quoteId + "/update";
  return http.put(urlData, quoteData, params);
}

/**
* Activates a quote and make it available to the system
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param quoteData quote data information
* @param params information for authentication
* @param quoteId quote id
* @return http result information
*/
export function putQuote(enviroment, branch, quoteData, params, quoteId){
  const urlData = enviromentURL[enviroment][branch] + urlQuote + quoteId + "/activate";
  return http.put(urlData, quoteData, params);
}

/**
* Update Adjust rates on a plan
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param adjustRateData adjust rate data information
* @param params information for authentication
* @param quoteId quote id
* @return http result information
*/
export function putQuote(enviroment, branch, adjustRateData, params, quoteId){
  const urlData = enviromentURL[enviroment][branch] + urlQuote + quoteId + "/adjust-rates";
  return http.put(urlData, adjustRateData, params);
}