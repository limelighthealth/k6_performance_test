/**
* A set of Whole case APIs.
*/
import http from "k6/http";

const urlGroup = "/v1/group/";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Get whole case for the group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getWholeCase(enviroment, branch, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/whole-case";
  return http.get(urlData, params);
}

/**
* Creates a Whole Case for the group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param groupData branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function postWholeCase(enviroment, branch, wholeCaseData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/whole-case";
  return http.post(urlData, wholeCaseData, params);
}

/**
* Updates a Whole Case for the group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param groupData branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function putWholeCase(enviroment, branch, wholeCaseData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/whole-case";
  return http.put(urlData, wholeCaseData, params);
}
