/**
* A set of Plan APIs.
*/
import http from "k6/http";

const urlGroup = "/v1/group";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Gets availability plan for the specified group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getPlanAvailability(enviroment, branch, params, groupId){
    const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/availability";
    return http.get(urlData, params);
  }

/**
* Create plan filters for the specified group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param planFilterData json file with group information
* @param params information for authentication
* @param groupId group id
*/
export function postPlanFilter(enviroment, branch, planFilterData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/filter";
  return http.post(urlData, planFilterData, params);
}

/**
* Create plan rates for the specified group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param planRateData plan rate data information
* @param params information for authentication
* @param groupId group id
*/
export function postPlanRate(enviroment, branch, planRateData, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/rate";
  return http.post(urlData, planRateData, params);
}

/**
* Get plans stored on a group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getPlan(enviroment, branch, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan";
  return http.get(urlData, params);
}

/**
* Create a plan to store on a group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param planData json file with group information
* @param params information for authentication
* @param groupId group id
* @param planId plan id
*/
export function postPlan(enviroment, branch, planData, params, groupId, planId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/" + planId;
  return http.post(urlData, planData, params);
}

/**
* Update a plan on the group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param planData json file with group information
* @param params information for authentication
* @param groupId group id
* @param planId plan id
*/
export function putPlan(enviroment, branch, planData, params, groupId, planId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/" + planId;
  return http.put(urlData, planData, params);
}

/**
* Delete a plan on the group
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param planData json file with group information
* @param params information for authentication
* @param groupId group id
* @param planId plan id
*/
export function deletePlan(enviroment, branch, planData, params, groupId, planId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/plan/" + planId;
  return http.del(urlData, planData, params);
}