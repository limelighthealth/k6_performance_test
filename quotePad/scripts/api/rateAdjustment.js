/**
* A set of rate adjustment APIs.
*/
import http from "k6/http";

const urlGroup = "/v1/group/";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Gets rate adjustments for quotes on the specified group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getRateAdjusments(enviroment, branch, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/rate-adjustments";
  return http.get(urlData, params);
}