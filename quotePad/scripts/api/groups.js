/**
* A set of Group APIs.
*/
import http from "k6/http";

const urlGroup = "/v1/group/";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Gets a group from an ID
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getGroup(enviroment, branch, params, groupId){
    const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId;
    return http.get(urlData, params);
}

/**
* Creates a group with group JSON info
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param groupData json file with group information
* @param params information for authentication
* @return http result information
*/
export function createGroup(enviroment, branch, groupData, params){
  const urlData = enviromentURL[enviroment][branch] + urlGroup;
  return http.post(urlData, groupData, params);
}

/**
* Delete Group by group id
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function deleteGroup(enviroment, branch, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId;
  return http.del(urlData, params);
}

/**
* Get Domain Counts
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @param groupId group id
* @return http result information
*/
export function getDomainCounts(enviroment, branch, params, groupId){
  const urlData = enviromentURL[enviroment][branch] + urlGroup + groupId + "/domain-counts";
  return http.get(urlData, params);
}