/**
* A set of Comment APIs.
*/
import http from "k6/http";

const urlCommentQuery= "/v1/comment/query";
const urlComment= "/v1/comment";
const enviromentURL = JSON.parse(open("../../config/environments.json"));

/**
* Gets comments query available
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @return http result information
*/
export function getCommentQuery(enviroment, branch, params){
  const urlData = enviromentURL[enviroment][branch] + urlCommentQuery;
  return http.get(urlData, params);
}

/**
* Gets comments available
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param params information for authentication
* @return http result information
*/
export function getComment(enviroment, branch, params){
  const urlData = enviromentURL[enviroment][branch] + urlComment;
  return http.get(urlData, params);
}

/**
* Creates comment with JSON info
* @param enviroment environment eg: perf-sandbox, automation
* @param branch branch eg: master, localhost
* @param commentInfo json file with comment information
* @param params information for authentication
*/
export function createComment(enviroment, branch, commentInfo, params){
  const urlData = enviromentURL[enviroment][branch] + urlComment;
  return http.post(urlData, commentInfo, params);
}