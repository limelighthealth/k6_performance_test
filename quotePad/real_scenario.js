import { login_scenario } from "./login_scenario.js";
import { comment_scenario } from "./comment_scenario.js";
import { create_group_scenario } from "./create_group_scenario.js";
let scenarioFile= __ENV.SCENARIO_FILE;
let scenarioName= __ENV.SCENARIO_NAME;
let scenarios=JSON.parse(open(scenarioFile));
let scenario =  scenarios[scenarioName]

export const options = {
  scenarios: {
    login: {
      executor: "shared-iterations",
      exec: "loginScenario",
      startTime: "0s",
      vus: scenario.vmu,
      iterations: 1,
    },
    comment: {
      executor: "shared-iterations",
      exec: "commentScenario",
      startTime: "0s",
      vus: scenario.vmu,
      iterations: 1,
    },
    group: {
      executor: "shared-iterations",
      exec: "createGroupScenario",
      startTime: "0s",
      vus: scenario.vmu,
      iterations: 1,
    }
  }
};

export function loginScenario() {
  login_scenario(scenario.environment, scenario.branch, scenario.user);
}

export function createGroupScenario() {
  create_group_scenario(scenario.environment, scenario.branch, scenario.user);
}

export function commentScenario() {
  comment_scenario(scenario.environment, scenario.branch, scenario.user);
}