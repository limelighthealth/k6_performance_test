import { check } from "k6";
import { loginWithCookies } from "./scripts/api/login.js";
import { getCommentQuery } from "./scripts/api/comment.js";

/**
* Gets a comment and check the correct status
*/
export function comment_scenario(environment, branch, user) {
  let params = loginWithCookies(environment, branch, user);
  const commentResponse = getCommentQuery(environment, branch, params);
  check(commentResponse, {
    "status is 200": (r) => r.status === 200,
  });
}
