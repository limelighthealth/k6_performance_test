
import { check } from "k6";
import { loginWithCookies } from "./scripts/api/login.js";
import { createGroup } from "./scripts/api/groups.js";
const groupData = JSON.parse(open("./testdata/data/groupData.json"));

/**
* create group and check the correct status
*/
export function create_group_scenario(environment, branch, user) {
  let params = loginWithCookies(environment, branch, user);

  const postGroupResp = createGroup(environment, branch, JSON.stringify(groupData), params);

  check(postGroupResp, {
    "status is 201": (r) => r.status === 201,
  });

}
