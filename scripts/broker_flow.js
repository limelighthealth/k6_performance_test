import { check, sleep } from "k6";
import { Trend, Rate } from "k6/metrics";
import { loginToApp } from "./postHelpers/postLogin.js";
import { getHome } from "./getHelpers/getHome.js";
import {
  getGroup,
  getGroupClasses,
  getGroupProperties,
  getGroupCarrier,
  getGroupPlans,
  getGroupQuotes,
} from "./getHelpers/getGroup.js";
import {
  baseParams,
  classParams,
  getCarrierParams,
  getQuoteFromGroupParams,
  getPlansFromGroupParams,
  getGroupPropertiesParams,
} from "./paramHelpers/paramHelper.js";
import { classDataBuilder } from "./payloadHelper/payloadHelper.js";

const GetHomeErrorRate = new Rate("Get home errors");
const GetHomeTrend = new Trend("Get home Trend");
const GetGroupErrorRate = new Rate("Get group errors");
const GetGroupTrend = new Trend("Get group Trend");

const GetGroupClassesErrorRate = new Rate("Get group classes errors");
const GetGroupClassesTrend = new Trend("Get group classes Trend");
const GetGroupPropertiesErrorRate = new Rate("Get group properties errors");
const GetGroupPropertiesTrend = new Trend("Get group properties Trend");
const GetCarriersErrorRate = new Rate("Get carriers errors");
const GetCarriersTrend = new Trend("Get carriers Trend");
const GetPlansErrorRate = new Rate("Get Plans errors");
const GetPlansTrend = new Trend("Get Plans Trend");

const GetQuoteErrorRate = new Rate("Get quote errors");
const GetQuoteTrend = new Trend("Get quote Trend");

export const options = {
  thresholds: {
    GetGroup: ["p(95)<500"],
  },
};

const selection = JSON.parse(open("./testdata/selection.json"));

const enviroment = "automation";

export default function () {
  broker_flow();
}

export function broker_flow() {
  let groupId = "624334daad0ce1000164ade6";
  let login_response = loginToApp(enviroment, "v2rates_broker");

  const token = login_response.cookies["auth-token"][0].value;
  const arrow = login_response.cookies["Arrow"][0].value;

  const params = baseParams(token, arrow);
  const getHomeResp = getHome(enviroment, params);
  const getGroupResp = getGroup(enviroment, params, groupId);

  check(getHomeResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetHomeErrorRate.add(1);

  GetHomeTrend.add(getHomeResp.timings.duration);

  check(getGroupResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupErrorRate.add(1);

  GetGroupTrend.add(getGroupResp.timings.duration);

  sleep(1);

  const whatClasses = classDataBuilder(groupId);
  const groupClassesParams = classParams(token, arrow, whatClasses);
  const groupPropertiesParams = getGroupPropertiesParams(
    token,
    arrow,
    selection,
    groupId
  );
  const groupCarriersParams = getCarrierParams(token, arrow);
  const groupPlansParams = getPlansFromGroupParams(token, arrow, groupId);

  const getGroupClassesResponse = getGroupClasses(
    enviroment,
    groupClassesParams
  );
  const getGroupPropertiesResponse = getGroupProperties(
    enviroment,
    groupPropertiesParams
  );
  const getGroupCarrierResponse = getGroupCarrier(
    enviroment,
    groupCarriersParams
  );
  const getGroupPlansResponse = getGroupPlans(
    enviroment,
    groupPlansParams,
    "624334daad0ce1000164ade6"
  );

  check(getGroupClassesResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupClassesErrorRate.add(1);

  GetGroupClassesTrend.add(getGroupClassesResponse.timings.duration);

  check(getGroupPropertiesResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupPropertiesErrorRate.add(1);

  GetGroupPropertiesTrend.add(getGroupPropertiesResponse.timings.duration);

  check(getGroupCarrierResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetCarriersErrorRate.add(1);

  GetCarriersTrend.add(getGroupCarrierResponse.timings.duration);

  check(getGroupPlansResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetPlansErrorRate.add(1);

  GetPlansTrend.add(getGroupPlansResponse.timings.duration);

  sleep(1);

  const getQuoteParams = getQuoteFromGroupParams(token, arrow, groupId);
  const getQuoteResp = getGroupQuotes(enviroment, getQuoteParams, groupId);

  check(getQuoteResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetQuoteErrorRate.add(1);

  GetQuoteTrend.add(getQuoteResp.timings.duration);
  sleep(1);
}
