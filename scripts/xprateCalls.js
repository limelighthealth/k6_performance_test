import http from "k6/http";

const enviromentURL = JSON.parse(open("./testdata/environments.json"));

export function putXprateTable(enviroment, params, quoteId){
  const urlData = enviromentURL[enviroment]['master'] + "/xprate/" + quoteId;
  return http.put(urlData, undefined, params);
}

export function getXprateTable(enviroment, params, quoteId){
  const urlData = enviromentURL[enviroment]['master'] + "/xprate/" + quoteId;
  return http.get(urlData, params);
}

export function putXprateValue(enviroment, data, params, quoteId){
  const urlData = enviromentURL[enviroment]['master'] + "/xprate/" + quoteId + "/cells";
  return http.put(urlData, data, params);
}

export function putXprateColumn(enviroment, param, quoteId){
  const urlData = enviromentURL[enviroment]['master'] + "/xprate/" + quoteId + "/column";
  return http.put(urlData, undefined, param);
}

export function deleteXprateColumn(enviroment, param, quoteId){
  const urlData = enviromentURL[enviroment]['master'] + "/xprate/" + quoteId + "/column";
  return http.del(urlData, undefined, param);
}