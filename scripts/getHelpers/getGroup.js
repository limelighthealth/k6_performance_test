import http from "k6/http";

const urlGroup = "/v1/group/";
const urlGroupList = "/v1/group/query?";
const urlGroupCarrier = "/api/carrier/query?";
const urlClasses = "/api/group_class/query?";
const urlGroupProperties = "/api/group/list?";
const enviromentURL = JSON.parse(open("../testdata/environments.json"));

export function getListGroup(enviroment, params){
  const urlData = enviromentURL[enviroment]['master'] + urlGroupList;
  return http.get(urlData, params);
}

export function getGroup(enviroment, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + urlGroup +groupId;
  return http.get(urlData, params);
}

export function getGroupCarrier(enviroment, params){
  const urlData = enviromentURL[enviroment]['master'] + urlGroupCarrier;
  return http.get(urlData, params);
}

export function getGroupClasses(enviroment, params){
  const urlData = enviromentURL[enviroment]['master'] + urlClasses;
  return http.get(urlData, params);
}

export function getGroupPlans(enviroment, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + '/api/' +groupId +'/get_plans?';
  return http.get(urlData, params);
}

export function getGroupProperties(enviroment, params){
  const urlData = enviromentURL[enviroment]['master'] + urlGroupProperties;
  return http.get(urlData, params);
}

export function getGroupQuotes(enviroment, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + '/v1/group/' +groupId +'/quotes?';
  return http.get(urlData, params);
}