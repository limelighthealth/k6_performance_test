import http from "k6/http";

const urlHome = "/home";
const enviromentURL = JSON.parse(open("../testdata/environments.json"));

export function getHome(enviroment, params){
  const urlData = enviromentURL[enviroment]['master'] + urlHome;
  return http.get(urlData, params);
}

