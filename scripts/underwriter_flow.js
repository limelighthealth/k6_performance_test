import { check, sleep } from "k6";
import { Trend, Rate } from "k6/metrics";
import { loginToApp } from "./postHelpers/postLogin.js";
import { getHome } from "./getHelpers/getHome.js";
import {
  getGroup,
  getGroupClasses,
  getGroupProperties,
  getGroupCarrier,
  getGroupPlans,
  getGroupQuotes,
} from "./getHelpers/getGroup.js";
import {
  postGroup,
  postMember,
  postPlan,
  postQuote,
  postProposal,
  postWholeCase,
} from "./postHelpers/postGroup.js";
import { putActivateProposal } from "./putHelpers/putActivateProposal.js";
import {
  baseParams,
  classParams,
  getCarrierParams,
  getQuoteFromGroupParams,
  getPlansFromGroupParams,
  getGroupPropertiesParams,
} from "./paramHelpers/paramHelper.js";
import {
  memberDataBuilder,
  classDataBuilder,
  proposalDataBuilder,
  quoteDataBuilder,
} from "./payloadHelper/payloadHelper.js";

const GetHomeErrorRate = new Rate("Get home errors");
const GetHomeTrend = new Trend("Get home Trend");
const PostGroupErrorRate = new Rate("Post group errors");
const PostGroupTrend = new Trend("Post group Trend");
const GetGroupErrorRate = new Rate("Get group errors");
const GetGroupTrend = new Trend("Get group Trend");

const GetGroupClassesErrorRate = new Rate("Get group classes errors");
const GetGroupClassesTrend = new Trend("Get group classes Trend");
const GetGroupPropertiesErrorRate = new Rate("Get group properties errors");
const GetGroupPropertiesTrend = new Trend("Get group properties Trend");
const GetCarriersErrorRate = new Rate("Get carriers errors");
const GetCarriersTrend = new Trend("Get carriers Trend");
const GetPlansErrorRate = new Rate("Get Plans errors");
const GetPlansTrend = new Trend("Get Plans Trend");

const PostPlanErrorRate = new Rate("Post plans errors");
const PostPlanTrend = new Trend("Post plan Trend");
const PostQuotesErrorRate = new Rate("Post quote errors");
const PostQuotesTrend = new Trend("Post quote Trend");
const GetQuoteErrorRate = new Rate("Get quote errors");
const GetQuoteTrend = new Trend("Get quote Trend");

const PostProposalErrorRate = new Rate("Post proposal errors");
const PostProposalTrend = new Trend("Post proposal Trend");
const AddWholecaseErrorRate = new Rate("Add whole case errors");
const AddWholecaseTrend = new Trend("Post add whole case Trend");

const groupData = JSON.parse(open("./testdata/groupData.json"));
const planData = JSON.parse(open("./testdata/planData.json"));
const selection = JSON.parse(open("./testdata/selection.json"));

const enviroment = "automation";

export default function () {
  underwriter_flow();
}

export function underwriter_flow() {
  let login_response = loginToApp(enviroment, "v2rates_underwriter");
  const token = login_response.cookies["auth-token"][0].value;
  const arrow = login_response.cookies["Arrow"][0].value;

  const params = baseParams(token, arrow);
  const getHomeResp = getHome(enviroment, params);
  const getGroupResp = getGroup(enviroment, params, "624334daad0ce1000164ade6");
  const postGroupResp = postGroup(
    enviroment,
    JSON.stringify(groupData),
    params
  );
  const groupId = JSON.parse(postGroupResp.body).data.id;
  console.log(groupId);

  check(getHomeResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetHomeErrorRate.add(1);

  GetHomeTrend.add(getHomeResp.timings.duration);

  check(getGroupResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupErrorRate.add(1);

  GetGroupTrend.add(getGroupResp.timings.duration);

  check(postGroupResp, {
    "status is 201": (r) => r.status === 201,
  }) || PostGroupErrorRate.add(1);

  PostGroupTrend.add(postGroupResp.timings.duration);

  sleep(1);

  const memberData = memberDataBuilder(groupId);

  postMember(enviroment, JSON.stringify(memberData), params);
  postMember(enviroment, JSON.stringify(memberData), params);
  postMember(enviroment, JSON.stringify(memberData), params);

  const whatClasses = classDataBuilder(groupId);
  const groupClassesParams = classParams(token, arrow, whatClasses);
  const groupPropertiesParams = getGroupPropertiesParams(
    token,
    arrow,
    selection,
    groupId
  );
  const groupCarriersParams = getCarrierParams(token, arrow);
  const groupPlansParams = getPlansFromGroupParams(token, arrow, groupId);

  const getGroupClassesResponse = getGroupClasses(
    enviroment,
    groupClassesParams
  );
  const getGroupPropertiesResponse = getGroupProperties(
    enviroment,
    groupPropertiesParams
  );
  const getGroupCarrierResponse = getGroupCarrier(
    enviroment,
    groupCarriersParams
  );
  const getGroupPlansResponse = getGroupPlans(
    enviroment,
    groupPlansParams,
    "624334daad0ce1000164ade6"
  );
  const postPlanResp = postPlan(
    enviroment,
    JSON.stringify(planData),
    params,
    groupId
  );
  const planId = JSON.parse(postPlanResp.body).data.id;
  const lineId = JSON.parse(postPlanResp.body).data['line-id'];

  check(getGroupClassesResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupClassesErrorRate.add(1);

  GetGroupClassesTrend.add(getGroupClassesResponse.timings.duration);

  check(getGroupPropertiesResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetGroupPropertiesErrorRate.add(1);

  GetGroupPropertiesTrend.add(getGroupPropertiesResponse.timings.duration);

  check(getGroupCarrierResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetCarriersErrorRate.add(1);

  GetCarriersTrend.add(getGroupCarrierResponse.timings.duration);

  check(getGroupPlansResponse, {
    "status is 200": (r) => r.status === 200,
  }) || GetPlansErrorRate.add(1);

  GetPlansTrend.add(getGroupPlansResponse.timings.duration);

  check(postPlanResp, {
    "status is 201": (r) => r.status === 201,
  }) || PostPlanErrorRate.add(1);

  PostPlanTrend.add(postPlanResp.timings.duration);

  sleep(1);

  const getQuoteParams = getQuoteFromGroupParams(token, arrow, groupId);
  const postQuoteData = quoteDataBuilder(planId, lineId, "Quote Long-Term Disability");

  const postQuoteResp = postQuote(
    enviroment,
    JSON.stringify(postQuoteData),
    params,
    groupId
  );
  const getQuoteResp = getGroupQuotes(enviroment, getQuoteParams, groupId);
  const quoteId = JSON.parse(postQuoteResp.body).data.id;

  check(postQuoteResp, {
    "status is 201": (r) => r.status === 201,
  }) || PostQuotesErrorRate.add(1);

  PostQuotesTrend.add(postQuoteResp.timings.duration);

  check(getQuoteResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetQuoteErrorRate.add(1);

  GetQuoteTrend.add(getQuoteResp.timings.duration);

  sleep(1);

  const postProposalData = proposalDataBuilder(groupId, quoteId);
  const postWholeCaesData = { "group-id": groupId, "quote-ids": [quoteId] };

  const addWholecaseResp = postWholeCase(
    enviroment,
    JSON.stringify(postWholeCaesData),
    params,
    groupId
  );
  const postProposalResp = postProposal(
    enviroment,
    JSON.stringify(postProposalData),
    params,
    groupId
  );
  const proposalID = JSON.parse(postProposalResp.body).data.id;

  check(addWholecaseResp, {
    "status is 200": (r) => r.status === 200,
  }) || AddWholecaseErrorRate.add(1);

  AddWholecaseTrend.add(addWholecaseResp.timings.duration);

  check(postProposalResp, {
    "status is 201": (r) => r.status === 201,
  }) || PostProposalErrorRate.add(1);

  PostProposalTrend.add(postProposalResp.timings.duration);
  sleep(1);

  putActivateProposal(
    enviroment,
    `{"proposal-id":"` + proposalID + `"}`,
    params,
    proposalID
  );
}
