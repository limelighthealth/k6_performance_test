import { underwriter_flow } from "./performance_flow.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = {

  scenarios: {
    performance_scenario: {
      executor: "shared-iterations",
      exec: "underwriter",
      startTime: "0s",
      tags: { example_tag: "underwritter" },
      vus: 10,
      iterations: 10,
    },
  },
};

export function underwriter() {
  underwriter_flow();
}

