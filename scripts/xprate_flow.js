import { check} from "k6";
import { Trend, Rate } from "k6/metrics";
import { loginToApp } from "./postHelpers/postLogin.js";

import * as postCalls from "./postHelpers/postGroup.js";
import * as xprateCalls from "./xprateCalls.js";
import * as param from "./paramHelpers/paramHelper.js";
import * as payload from "./payloadHelper/payloadHelper.js";

const PutXprateTableErrorRate = new Rate("Put experience rating table errors");
const PutXprateTableTrend = new Trend("Put experience rating table Trend");
const GetXprateTableErrorRate = new Rate("Get experience rating table errors");
const GetXprateTableTrend = new Trend("Get experience rating table Trend");
const PutXprateValueErrorRate = new Rate("Put experience rating value errors");
const PutXprateValueTrend = new Trend("Put experience rating value Trend");
const PutXprateColumnErrorRate = new Rate("Put experience rating column errors");
const PutXprateColumnTrend = new Trend("Put experience rating column Trend");
const DeleteXprateColumnErrorRate = new Rate("Delete experience rating column errors");
const DeleteXprateColumnTrend = new Trend("Delete experience rating column Trend");

const groupData = JSON.parse(open("./testdata/groupData.json"));
const planData = JSON.parse(open("./testdata/planDataCI.json"));
const xprateData = JSON.parse(open("./testdata/xprateData.json"));

const enviroment = "automation";

export function xprate_flow(){
  let login_response = loginToApp(enviroment, "v2rates_xprate_underwriter");
  const token = login_response.cookies["auth-token"][0].value;
  const arrow = login_response.cookies["Arrow"][0].value;

  const params = param.baseParams(token, arrow);

  const postGroupResp = postCalls.postGroup(
    enviroment,
    JSON.stringify(groupData),
    params
  );
  const groupId = JSON.parse(postGroupResp.body).data.id;
  console.log(groupId);

  const memberData = payload.memberDataBuilder(groupId);

  postCalls.postMember(enviroment, JSON.stringify(memberData), params);
  postCalls.postMember(enviroment, JSON.stringify(memberData), params);
  postCalls.postMember(enviroment, JSON.stringify(memberData), params);

  const postPlanResp = postCalls.postPlan(
    enviroment,
    JSON.stringify(planData),
    params,
    groupId
  );
  const planId = JSON.parse(postPlanResp.body).data.id;
  const lineId = JSON.parse(postPlanResp.body).data['line-id'];

  const postQuoteData = payload.quoteDataBuilder(planId, lineId, "CI quote");

  const postQuoteResp = postCalls.postQuote(
    enviroment,
    JSON.stringify(postQuoteData),
    params,
    groupId
  );

  const quoteId = JSON.parse(postQuoteResp.body).data.id;

  const putXprateTableResp = xprateCalls.putXprateTable(enviroment, param, quoteId);

  check(putXprateTableResp, {
    "status is 200": (r) => r.status === 200,
  }) || PutXprateTableErrorRate.add(1);
  check(putXprateTableResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || PutXprateTableErrorRate.add(1);

  PutXprateTableTrend.add(putXprateTableResp.timings.duration);

  const getXprateTableResp = xprateCalls.getXprateTable(enviroment, params, quoteId);
  
  check(getXprateTableResp, {
    "status is 200": (r) => r.status === 200,
  }) || GetXprateTableErrorRate.add(1);
  check(getXprateTableResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || GetXprateTableErrorRate.add(1);

  GetXprateTableTrend.add(getXprateTableResp.timings.duration);

  const putXprateMultipleValuesResp = xprateCalls.putXprateValue(
    enviroment, 
    JSON.stringify(xprateData), 
    params, 
    quoteId
  );

  check(putXprateMultipleValuesResp, {
    "status is 200": (r) => r.status === 200,
  }) || PutXprateValueErrorRate.add(1);
  check(putXprateMultipleValuesResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || PutXprateValueErrorRate.add(1);

  PutXprateValueTrend.add(putXprateMultipleValuesResp.timings.duration);

  const putXprateValueData = payload.putXprateValueDataBuilder('PREM', 0, 10000);

  const putXprateValueResp = xprateCalls.putXprateValue(
    enviroment, 
    JSON.stringify(putXprateValueData), 
    params, 
    quoteId
  );

  check(putXprateValueResp, {
    "status is 200": (r) => r.status === 200,
  }) || PutXprateValueErrorRate.add(1);
  check(putXprateValueResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || PutXprateValueErrorRate.add(1);

  PutXprateValueTrend.add(putXprateValueResp.timings.duration);
  
  const putXprateColumnResp = xprateCalls.putXprateColumn(enviroment, params, quoteId);
  check(putXprateColumnResp, {
    "status is 200": (r) => r.status === 200,
  }) || PutXprateColumnErrorRate.add(1);
  check(putXprateColumnResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || PutXprateColumnErrorRate.add(1);

  PutXprateColumnTrend.add(putXprateColumnResp.timings.duration);
  
  const deleteXprateColumnResp = xprateCalls.deleteXprateColumn(enviroment, params, quoteId);
  check(deleteXprateColumnResp, {
    "status is 200": (r) => r.status === 200,
  }) || DeleteXprateColumnErrorRate.add(1);
  check(deleteXprateColumnResp, {
    "verify resp payload contains text": (r) => r.body.includes("config"),
  }) || DeleteXprateColumnErrorRate.add(1);

  DeleteXprateColumnTrend.add(deleteXprateColumnResp.timings.duration);

}