import { underwriter_flow } from "./underwriter_flow.js";
import { broker_flow } from "./broker_flow.js";
import { sales_executive_flow } from "./sales_executive_flow.js";
import { xprate_flow } from "./xprate_flow.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = {

  scenarios: {
    underwritter_scenario: {
      executor: "shared-iterations",
      exec: "underwriter",
      startTime: "0s",
      tags: { example_tag: "underwritter" },
      vus: 10,
      iterations: 20,
    },
    broker_scenario: {
      executor: "shared-iterations",
      exec: "broker",
      startTime: "0s",
      tags: { example_tag: "broker" },
      vus: 20,
      iterations: 40,
    },
    sales_executive_scenario: {
      executor: "shared-iterations",
      exec: "sales_executive",
      startTime: "0s",
      tags: { example_tag: "sales_executive" },
      vus: 1,
      iterations: 1,
    },
    xprate_scenario:{
      executor: "shared-iterations",
      exec: "xprate",
      startTime: "0s",
      tags: { example_tag: "xprate" },
      vus: 1,
      iterations: 1,
    }
  },
};

export function underwriter() {
  underwriter_flow();
}
export function broker() {
  broker_flow();
}
export function sales_executive() {
  sales_executive_flow();
}
export function xprate(){
  xprate_flow();
}