export function quoteDataBuilder(planId, lineId, quoteName) {
  const postQuoteData = {
    "line-id": lineId,
    "rate-structure-type": "v2-rate",
    "plan-descriptors": [{ "plan-id": planId }],
    name: quoteName,
    "effective-on": "2022-05-01T00:00:00.000Z",
    "member-assignment-strategy": { type: "unspecified" },
  };
  return postQuoteData;
}
export function proposalDataBuilder(groupId, quoteId) {
  const postProposalData = {
    name: "new Proposal",
    "group-id": groupId,
    "quote-ids": [quoteId],
    options: { "quote-level-options": {} },
  };
  return postProposalData;
}

export function memberDataBuilder(groupId) {
  const memberData = {
    group_id: groupId,
    class_number: 1,
    first_name: "Employee",
    last_name: "six",
    dob: "1987-11-06T00:00:00.000Z",
    age: 34,
    sex: "M",
    coverage_codes: { 1: "EE", 2: "EE", 13: "EE" },
    w2_income: 2000,
    postal_code: "90001",
    city: "Los Angeles",
    medical_region_id: 16,
    region: "CA",
    county: "Los Angeles",
    county_code: 37,
    available_counties: [],
    available_county_codes: [],
    available_medical_region_ids: [],
    has_county_selected: true,
    is_out_of_state: false,
    salary_mode: "monthly",
    pay_type: "hourly",
    payroll_method: "weekly",
    employment_status: "Full Time",
  };
  return memberData;
}

export function classDataBuilder(groupId) {
  const whatClasses = {
    group_id: groupId,
    line_id: { $in: [13, 1, 2, 3, 4, 7, 8, 11, 15] },
    is_deleted: false,
    is_archived: false,
  };
  return whatClasses;
}

export function putXprateValueDataBuilder(rowName, columnIndex, value){
  const setXprateValueData = [
    {
      "rowName": rowName,
      "columnIndex": columnIndex,
      "value": value
    }
  ];
  return setXprateValueData;
}
