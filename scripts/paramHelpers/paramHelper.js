export function baseParams(token, arrow) {
  const params = {
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return params;
}

export function groupListParams(token, arrow, searchOptions, searchColumns) {
  const groupListParams = {
    "page-size": "25",
    "group-by": "group-name",
    "select-options": JSON.stringify(searchOptions),
    "search-columns": JSON.stringify(searchColumns),
      auth: "basic",
      cookies: {
        "auth-token": token,
        Arrow: arrow,
      },
      headers: {
        "Content-Type": "application/json",
      },
    };
  return groupListParams;
}

export function classParams(token, arrow, whatClasses) {
  const groupClassesParams = {
    limit: 1000,
    where: JSON.stringify(whatClasses),
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return groupClassesParams;
}

export function getCarrierParams(token, arrow) {
  const groupCarriersParams = {
    limit: 1000,
    where: JSON.stringify({ id: { $in: [243] } }),
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return groupCarriersParams;
}

export function getGroupPropertiesParams(token, arrow, selection, groupId) {
  const groupPropertiesParams = {
    brokers: true,
    sales_executives: true,
    sel: JSON.stringify(selection),
    where: JSON.stringify({ id: groupId }),
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return groupPropertiesParams;
}

export function getPlansFromGroupParams(token, arrow, groupId) {
  const groupPlansParams = {
    id: groupId,
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return groupPlansParams;
}

export function getQuoteFromGroupParams(token, arrow, groupId) {
  const quoteParams = {
    "group-id": groupId,
    auth: "basic",
    cookies: {
      "auth-token": token,
      Arrow: arrow,
    },
    headers: {
      "Content-Type": "application/json",
    },
  };
  return quoteParams;
}
