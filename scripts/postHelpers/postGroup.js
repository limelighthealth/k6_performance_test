import http from "k6/http";

const urlGroup = "/v1/group";
const enviromentURL = JSON.parse(open("../testdata/environments.json"));

export function postGroup(enviroment, groupData, params){
  const urlData = enviromentURL[enviroment]['master'] + urlGroup;
  return http.post(urlData, groupData, params);
}

const urlMember = "/api/member";

export function postMember(enviroment, data, params){
  const urlData = enviromentURL[enviroment]['master'] + urlMember;
  return http.post(urlData, data, params);
}

const urlV1 = "/v1/group/";

export function postPlan(enviroment, data, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + urlV1 + groupId + "/plan";
  return http.post(urlData, data, params);
}

export function postProposal(enviroment, data, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + urlV1 + groupId + "/proposal";
  return http.post(urlData, data, params);
}

export function postQuote(enviroment, data, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + urlV1 + groupId + "/quote";
  return http.post(urlData, data, params);
}

export function postWholeCase(enviroment, data, params, groupId){
  const urlData = enviromentURL[enviroment]['master'] + urlV1 + groupId + "/whole-case";
  return http.post(urlData, data, params);
}


