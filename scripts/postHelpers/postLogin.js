import http from "k6/http";

const urlLogin = "/api/user/login";
const users = JSON.parse(open("../testdata/users.json"));
const enviromentURL = JSON.parse(open("../testdata/environments.json"));

export function loginToApp(enviroment, user){
  const userAndPassFile =users[enviroment][user];
  const userAndPass = {
    email: userAndPassFile.username ,
    password: userAndPassFile.password
  };
  const urlData = enviromentURL[enviroment]['master'] + urlLogin;
  const login_response = http.post(urlData, userAndPass);
  return login_response;
}

