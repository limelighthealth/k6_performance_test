import http from "k6/http";

const urlV1 = "/v1/proposal/";
const enviromentURL = JSON.parse(open("../testdata/environments.json"));

export function putActivateProposal(enviroment, data, params, proposalID){
  const urlData = enviromentURL[enviroment]['master'] + urlV1 + proposalID + "/activate";
  return http.put(urlData, data, params);
}

