# K6 POC

This is an example on how we could implment performance test on K6.

## Running the test

Make sure you have k6 installed. Open a command prompt then go to the scripts folder and type in the following command:

`k6 run realScenarios.js`

The above script will run the test with 30 virtual users 20 will be brokers and 10 underwritters.

To learn more about the API check the other files. Also check https://k6.io/docs/javascript-api/

## Running tests with docker and showing results in grafana

* `docker-compose up -d influxdb grafana`
* e.g., `docker-compose run --rm k6 run /scripts/underwriter_flow.js`
